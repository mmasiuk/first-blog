FROM python:2-onbuild


EXPOSE 8080
COPY . /awesomeproject
CMD ["python","/awesomeproject/manage.py","runserver","0.0.0.0:8080"]
